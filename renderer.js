// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
console.log("Got here")
var zerorpc = require("zerorpc");
console.log("Initialized")
var response = require("./response.js");

var server = new zerorpc.Server({
    hearBeat:function(hearBeat,reply)
    {


        console.log("hearBeat:"+hearBeat);
        const hearBeatResponse = JSON.parse(hearBeat);
        window.$("#main-container").css({"backgroundColor" : "#000"});
        if( hearBeatResponse.state.toLowerCase() === "start" ) {
        	window.$("#container").show();
        	window.$("#container").addClass("active");
        } 
        if( hearBeatResponse.state.toLowerCase() === "poserfound" ){
        	const containerName = window.$("#main-container").find(".active");
        	console.log(containerName)
	    	window.$("#container").hide();
	    	window.$("#count-container").show();
	    	window.$("#count").text(hearBeatResponse.message);
			window.$("#counterText").text(hearBeatResponse.status);
        } 
        if( hearBeatResponse.state.toLowerCase() === "taking_photo" ){
        	window.$("#count-container").hide();
        	window.$("#taking-photo-container").show();
        	window.$("#taking-photo-content").text(hearBeatResponse.message);
        	window.$("#taking-photo-container").fadeOut(1000);
        }
        if( hearBeatResponse.state.toLowerCase() === "analysing" ) {
         	setTimeout( function(){ 
                window.$("#taking-photo-container").hide();
				window.$("#loading-container").show();
        	}, 800);
        }
        if( hearBeatResponse.state.toLowerCase() === "moved" ) {
        	window.$("#taking-photo-container").show();
        	window.$("#taking-photo-content").text(hearBeatResponse.message);
        }
        if( hearBeatResponse.state.toLowerCase() === "coldstop" ){
    	 	window.$("#fashion-container").hide();
        	window.$("#container").show();
        }
		
        reply(null,"Done")
    },
    styleu: function(styleresponse, reply) {
        console.log("Style:"+styleresponse);
        console.log(styleresponse)
       	//response = JSON.parse(styleresponse);
       	
        //Score key
        const score = response.analysisJson.scores.overall_score;
        window.$("#score").text(score);

        //Picutre attribute
        let timeStamp    = Math.floor(Date.now()/1000);
        window.$("#profile-pic").html("<img src='./image"+timeStamp+".jpg' />");

        //Top category and attributes
        const topCategory = response.analysisJson.analysis[0].category;
        const topAttributeList = response.analysisJson.analysis[0].attribute_list;

        window.$("#top-category").text(topCategory);
    	window.$("#top-attributes").html(getAttributes(topAttributeList));

    	//Bottom category and attribute
    	const bottomCategory = response.analysisJson.analysis[0].category;
        const bottomAttributeList = response.analysisJson.analysis[0].attribute_list;
        //const bottomCategory = null;
        //const bottomAttributeList = null;
        if( bottomCategory && bottomAttributeList ) {
	        window.$("#bottom-category").text(bottomCategory);
	    	window.$("#bottom-attributes").html(getAttributes(bottomAttributeList));
    	} else {
    		window.$("#bottom-container").hide();
    		window.$("#top-container").css({"border" : "0px"});
    	}

    	//Recommendation category and attribute
    	const recommendationCategory      = response.analysisJson.analysis[0].category;
        const recommendationAttributeList = response.analysisJson.analysis[0].attribute_list;
        window.$("#recommendation-category").text(recommendationCategory);
    	window.$("#recommendation-attributes").html(getAttributes(recommendationAttributeList));

    	//Show/ hide logo and detail container
	    window.$("#loading-container").hide();
	    window.$("#main-container").css({"backgroundColor" : "white"});
	    window.$("#fashion-container").show();

        reply(null, "Done");
    }
});
server.bind("tcp://0.0.0.0:4242");

function getAttributes( attributeList ) {
	let attributeListTag =  '';
    attributeList.map( attribute => {
		attributeListTag += "<div class='attributes'><p>"+attribute+"</p></div>"
	});
	attributeList.map( attribute => {
		attributeListTag += "<div class='attributes'><p>"+attribute+"</p></div>"
	});
	return attributeListTag;
}