import cv2
import sys
from datetime import datetime
import time
import numpy
import requests
import base64
import json
import threading
import zerorpc





DS_URL = 'http://192.168.12.239:8082/analyze/deviceid'
styleUurl='http://54.251.153.13:18080/analyze/'
faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

video_capture = cv2.VideoCapture(0)
faceFound = False
faceFoundTime = datetime.now()
oldfaces = None
findFaceTime = datetime.now()

bFaceCheckInProgress = False
PHOTO_TRIGGER_SECONDS = 3   
MOVEMENT_THRESHOLD = 100
COLDSTOP = 5   #We can change later to 30

def sendActivity(state,status,message):
    #print(state)
    #print(status)
    #print(message)
    #print('------------')
    #nodeconnection = zerorpc.Client()
    #nodeconnection.connect("tcp://127.0.0.1:4242")
    heartBeatPayload = {'state':state,'status':status,'message':message}
    print(json.dumps(heartBeatPayload))
    hbconnection = zerorpc.Client()
    hbconnection.connect("tcp://127.0.0.1:4242")
    hbconnection.hearBeat(json.dumps(heartBeatPayload))  

class CheckFaces(threading.Thread):
    def __init__(self,frame):
        super(CheckFaces,self).__init__()
        self.frame = frame
    def run(self):
        global oldfaces
        global findFaceTime
        global faceFound
        global faceFoundTime,bFaceCheckInProgress,styleUurl
        #global nodeconnection
        
        findFaceTime = datetime.now()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        if(faces is None or len(faces)== 0):
            print("No One  Detected")
            if(oldfaces is not None and len(oldfaces)> 0):
                print ("Just Lost some one")
                faceFound = False
                sendActivity('Start','Start','Just Lost some one')
        
        if(oldfaces is None or len(oldfaces)!= len(faces)):
            print(" Skip Faces",len(faces))
            if oldfaces is not None :
                print("Skip OldFaces",len(oldfaces))
            else:
                print("Someone Detected")
                #sendActivity('POSERFOUND','Start','Poser Found')
            if(len(faces)> 0):
                faceFoundTime = datetime.now()
            oldfaces = faces
        elif len(faces) == 1:
            x = sum(sum(numpy.subtract(oldfaces,faces)))
            print ("X",x)

            if(abs(x) <= MOVEMENT_THRESHOLD):
                if(faceFound == False):
                    diffTime = faceFoundTime- datetime.now()
                    delta = int(round(abs(diffTime.total_seconds())))
                    print ("Delta",delta)
                    # SEND Photo Trigger - Delta as time tick

                    if(delta<2):
                        sendActivity('POSERFOUND','GET',3)
                    elif(delta<3):
                        sendActivity('POSERFOUND','SET',2)
                    elif (delta <4):
                        sendActivity('POSERFOUND','POSE',1)

                    

                    if(delta > PHOTO_TRIGGER_SECONDS):
                        faceFound = True
                        #for index,(x, y, w, h) in enumerate(faces):
                        # PHOTO TAKEN
                        sendActivity('TAKING_PHOTO','POSE','Taking photo')
                        fileName = "image%04i.jpg" %time.time()
                        cv2.imwrite(fileName, frame)
                        image_data = open(fileName,'rb').read()
                        encodedimage = base64.b64encode(image_data)
                        styleUurl =   styleUurl + 'pi'
                        headers = { 'Content-type' : 'application/x-www-form-urlencoded; charset=UTF-8'}
                        sendActivity('ANALYSING','ANALYSIS','ANALYSING PHOTO')
                        #response = requests.request("POST", styleUurl, data={'image':encodedimage}, headers=headers)
                        #print (response.text)
                        # print (json.dumps(json.loads(r.text), sort_keys=True,indent=4, separators=(',', ': ')))
                        print('Check completed')  
                        nodeconnection = zerorpc.Client()
                        nodeconnection.connect("tcp://127.0.0.1:4242")
                        nodeconnection.styleu("response.text")  
                        # system thread sleep
                        time.sleep(COLDSTOP)
                        print('cold sleep')
                        sendActivity('COLDSTOP','COLDSTOP','Next Poser')
                        print('Spring wakeup')
                        faceFound = False

            else:
                print("Moved")
                sendActivity('MOVED','MOVED','POSER MOVED')
                faceFound = False
                faceFoundTime = datetime.now()
        else:
            print("too many faces",len(faces));
            sendActivity('MANY_FACES','MANY_FACES',len(faces))
                
        oldfaces = faces
        print("Faces",len(faces))
        print("OldFaces",len(oldfaces))
        

        bFaceCheckInProgress = False
        


class CheckStyle(threading.Thread):
    def __init__(self, data = None):
        super(CheckStyle, self).__init__()
        self.image_data = data

    def run(self):
        headers = { 'Content-type' : 'multipart/form-data'}
        encoded_data = base64.b64encode(self.image_data)
        postRequest = {"imgData" : encoded_data.decode("utf-8") }
        r = requests.post(DS_URL,json=postRequest)
        print (json.dumps(json.loads(r.text), sort_keys=True,indent=4, separators=(',', ': ')))
        print('Check completed')
        bFaceCheckInProgress = False

# raspberry

def print_style_details(frame):
    print('Processing')
    sendActivity('POSERFOUND','PHOTO_TAKEN','PHOTO TAKEN')
    fileName = "image%04i.jpg" %time.time()
    cv2.imwrite(fileName, frame)
    with open(fileName,'rb') as p:
        image_data = p.read()
        cs = CheckStyle(image_data)
        cs.start()



    

cf = None
while True:
    # Capture frame-by-frame
    if bFaceCheckInProgress:
        time.sleep(1)
        continue

    print ("Getting Photo from Camera")

    ret, frame = video_capture.read()
    if (frame is None or len(frame) ==0):
        print ("No Video, Sleeping for some time")
        sendActivity('NOACTIVITY','Waiting','Camera not found')
        time.sleep(5)
        continue

    lastchecked=datetime.now()-findFaceTime

    if lastchecked.total_seconds() > 1 and bFaceCheckInProgress==False:
        print ("Check for Face")
        bFaceCheckInProgress = True
        cf = CheckFaces(frame)
        cf.start()
    
    # Display the resulting frame
    #cv2.imshow('Video', frame)
    # Ideally We should sleep for a second


    if cv2.waitKey(1) & 0xFF == ord('q'):
        cf.kill()
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()


