module.exports = {
    "analysisJson": {
        "scores": {
            "overall_score": 91
        },
        "analysis": [
            {
                "colour": "#222221",
                "bounding_box": [
                    167,
                    386,
                    926,
                    1417
                ],
                "category": "TSHIRT",
                "attribute_list": [
                    "PULLOVER",
                    "PRINTED",
                    "LONG SLEEVES",
                    "ROUND NECK"
                ],
                "recommendation": {
                    "reco_type": "not_available"
                },
                "category_score": 88,
                "similar_styles": [
                    "S1",
                    "S2",
                    "S3",
                    "S4",
                    "S5"
                ]
            }
        ]
    },
    "scoreAnalysis": {
        "badge": {
            "badgeOneLine": "Stay Fashionable till the next time I see you.",
            "badgeCategory": "Beginners Luck",
            "badgeName": "Fashionista",
            "badgeUrlH": "",
            "badgeUrlL": "",
            "earnedBadge": 2,
            "foundBadge": true,
            "badgeText": "Congratulations on winning your first Badge!"
        },
        "badgeNudge": {
            "badgeNudgeText": null
        },
        "score": {
            "scoreAdjective": "Top 90 Percentile"
        },
        "oneline": {
            "oneLinerText": "Are you a 90-degree angle? Cause you are looking right",
            "onelinercategory": "Compliments"
        }
    },
    "faceAttribute": {
        "faceId": "df53a7ba-3c7a-4e29-84be-a81fb8f9773d",
        "gender": "male"
    },
    "deviceId": null,
    "gender": "male",
    "looks": {
        "topwearLookrecommendation": {
            "count": 5,
            "totalCount": 0,
            "valid": true,
            "limit": 5,
            "looks": [
                {
                    "articles": {
                        "Heels": {
                            "brandName": "Inc 5",
                            "discountedPrice": 1590,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1213946/2016/3/10/11457599683183-Inc5-Women-Black-Heels-9801457599681899-1.jpg",
                            "id": 1213946,
                            "productDisplayName": "Inc.5 Women Black Kitten Heels"
                        },
                        "Tops": {
                            "brandName": "RARE",
                            "discountedPrice": 999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1109296/2015/12/4/11449226419906-RARE-Rust-Red-Printed-Off-Shoulder-Blouson-Top-1521449226419390-1.jpg",
                            "id": 1109296,
                            "productDisplayName": "RARE Rust Red Printed Off-Shoulder Blouson Top"
                        },
                        "Handbags": {
                            "brandName": "Lavie",
                            "discountedPrice": 3040,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/image/style/properties/814683/Lavie-Black-Handbag_1_94c369b2825e6bddb48810a6e092dea3.jpg",
                            "id": 814683,
                            "productDisplayName": "Lavie Black Handbag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/11/17/1147939539923011449226419906-RARE-Rust-Red-Printed-Off-Shoulder-Blouson-Top-1521449226419390-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1109296,
                        814683,
                        1051162,
                        1213946
                    ],
                    "source": "VSBot",
                    "look_id": "f4c05d75-db99-42d3-9fdc-050722f80c3a",
                    "createdBy": "VSBot",
                    "styleid": 1109296,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Heels": {
                            "brandName": "Inc 5",
                            "discountedPrice": 2090,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1123787/2016/1/27/11453888684430-Inc-5-Women-Casual-Shoes-8801453888684321-1.jpg",
                            "id": 1123787,
                            "productDisplayName": "Inc 5 Women Black Wedges"
                        },
                        "Tops": {
                            "brandName": "Flying Machine",
                            "discountedPrice": 699,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1198157/2016/2/24/11456296278568-Flying-Machine-Women-Tops-9021456296278060-1.jpg",
                            "id": 1198157,
                            "productDisplayName": "Flying Machine Blue Printed Tank Top"
                        },
                        "Handbags": {
                            "brandName": "Phive Rivers",
                            "discountedPrice": 5699,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1058209/2015/11/3/11446550942993-Phive-Rivers-Black-Leather-Printed-Sling-Bag-9771446550939972-1.jpg",
                            "id": 1058209,
                            "productDisplayName": "Phive Rivers Black Leather Printed Sling Bag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/19/1148214913746211456296278568-Flying-Machine-Women-Tops-9021456296278060-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1198157,
                        1058209,
                        1051162,
                        1123787
                    ],
                    "source": "VSBot",
                    "look_id": "5e1981c7-be01-496b-9656-9ebe135e6195",
                    "createdBy": "VSBot",
                    "styleid": 1198157,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Heels": {
                            "brandName": "DressBerry",
                            "discountedPrice": 2899,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1475475/2016/8/30/11472556062921-DressBerry-Women-Heels-9551472556062637-1.jpg",
                            "id": 1475475,
                            "productDisplayName": "Dressberry Women Nude-Coloured Slim Heels"
                        },
                        "Tops": {
                            "brandName": "Trend Arrest",
                            "discountedPrice": 999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1355501/2016/5/19/11463644245064-Trend-Arrest-Blue-Layered-Top-201463644244861-1.jpg",
                            "id": 1355501,
                            "productDisplayName": "Trend Arrest Red Printed Crop Top"
                        },
                        "Handbags": {
                            "brandName": "Butterflies",
                            "discountedPrice": 2399,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/images/style/properties/Butterflies-Light-Brown-Handbag_47f4d3b664c3c6d057ed141a09adf81f_images.jpg",
                            "id": 432012,
                            "productDisplayName": "Butterflies Light Brown Handbag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/19/1148216402305011463644245064-Trend-Arrest-Blue-Layered-Top-201463644244861-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1355501,
                        432012,
                        1051162,
                        1475475
                    ],
                    "source": "VSBot",
                    "look_id": "e0b196eb-938f-461b-8801-51c0bc689792",
                    "createdBy": "VSBot",
                    "styleid": 1355501,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Tshirts": {
                            "brandName": "ONLY",
                            "discountedPrice": 695,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1219840/2016/2/23/11456209850101-ONLY-Women-Tshirts-2071456209849576-1.jpg",
                            "id": 1219840,
                            "productDisplayName": "ONLY Grey Melange T-shirt"
                        },
                        "Flats": {
                            "brandName": "Hush Puppies",
                            "discountedPrice": 1999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1517027/2016/9/29/11475143598031-Hush-Puppies-Women-Pink-Woven-Sandals-7511475143597803-1.jpg",
                            "id": 1517027,
                            "productDisplayName": "Hush Puppies Women Pink Woven Flats"
                        },
                        "Handbags": {
                            "brandName": "Eske",
                            "discountedPrice": 9499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1310090/2016/4/19/11461041848022-Eske-Women-Handbags-2671461041847585-1.jpg",
                            "id": 1310090,
                            "productDisplayName": "Eske Black & White Printed Leather Handbag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/19/1148217136963811456209850101-ONLY-Women-Tshirts-2071456209849576-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1219840,
                        1310090,
                        1051162,
                        1517027
                    ],
                    "source": "VSBot",
                    "look_id": "69c3419c-e5d8-4db6-a537-eaa5bee7f853",
                    "createdBy": "VSBot",
                    "styleid": 1219840,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Heels": {
                            "brandName": "Inc 5",
                            "discountedPrice": 1590,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1464737/2016/8/25/11472120695678-Inc-5-Women-Brown-Heels-1951472120695438-1.jpg",
                            "id": 1464737,
                            "productDisplayName": "Inc 5 Women Brown Heels"
                        },
                        "Tops": {
                            "brandName": "DressBerry",
                            "discountedPrice": 1099,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1265633/2016/4/22/11461322917687-DressBerry-Orange-Floral-Print-Top-6471461322917211-1.jpg",
                            "id": 1265633,
                            "productDisplayName": "DressBerry Coral Red Floral Printed Polyester Top"
                        },
                        "Handbags": {
                            "brandName": "GUESS",
                            "discountedPrice": 9999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1468003/2016/9/7/11473223788964-GUESS-Black-Glossy-Quilted-Shoulder-Bag-3381473223788789-1.jpg",
                            "id": 1468003,
                            "productDisplayName": "GUESS Black Glossy Quilted Shoulder Bag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/20/1148219105677511461322917687-DressBerry-Orange-Floral-Print-Top-6471461322917211-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1265633,
                        1468003,
                        1051162,
                        1464737
                    ],
                    "source": "VSBot",
                    "look_id": "42bc5c98-d48b-4102-bb26-8f2bb0b5e9fb",
                    "createdBy": "VSBot",
                    "styleid": 1265633,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                }
            ],
            "format": "tbaf",
            "present_styles": [
                "1051162"
            ],
            "offset": 0,
            "status": {
                "code": 3
            },
            "aregenerated": false
        },
        "bottomwearLookrecommendation": {
            "count": 5,
            "totalCount": 0,
            "valid": true,
            "limit": 5,
            "looks": [
                {
                    "articles": {
                        "Heels": {
                            "brandName": "Inc 5",
                            "discountedPrice": 1590,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1213946/2016/3/10/11457599683183-Inc5-Women-Black-Heels-9801457599681899-1.jpg",
                            "id": 1213946,
                            "productDisplayName": "Inc.5 Women Black Kitten Heels"
                        },
                        "Tops": {
                            "brandName": "RARE",
                            "discountedPrice": 999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1109296/2015/12/4/11449226419906-RARE-Rust-Red-Printed-Off-Shoulder-Blouson-Top-1521449226419390-1.jpg",
                            "id": 1109296,
                            "productDisplayName": "RARE Rust Red Printed Off-Shoulder Blouson Top"
                        },
                        "Handbags": {
                            "brandName": "Lavie",
                            "discountedPrice": 3040,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/image/style/properties/814683/Lavie-Black-Handbag_1_94c369b2825e6bddb48810a6e092dea3.jpg",
                            "id": 814683,
                            "productDisplayName": "Lavie Black Handbag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/11/17/1147939539923011449226419906-RARE-Rust-Red-Printed-Off-Shoulder-Blouson-Top-1521449226419390-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1109296,
                        814683,
                        1051162,
                        1213946
                    ],
                    "source": "VSBot",
                    "look_id": "f4c05d75-db99-42d3-9fdc-050722f80c3a",
                    "createdBy": "VSBot",
                    "styleid": 1109296,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Heels": {
                            "brandName": "Inc 5",
                            "discountedPrice": 2090,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1123787/2016/1/27/11453888684430-Inc-5-Women-Casual-Shoes-8801453888684321-1.jpg",
                            "id": 1123787,
                            "productDisplayName": "Inc 5 Women Black Wedges"
                        },
                        "Tops": {
                            "brandName": "Flying Machine",
                            "discountedPrice": 699,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1198157/2016/2/24/11456296278568-Flying-Machine-Women-Tops-9021456296278060-1.jpg",
                            "id": 1198157,
                            "productDisplayName": "Flying Machine Blue Printed Tank Top"
                        },
                        "Handbags": {
                            "brandName": "Phive Rivers",
                            "discountedPrice": 5699,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1058209/2015/11/3/11446550942993-Phive-Rivers-Black-Leather-Printed-Sling-Bag-9771446550939972-1.jpg",
                            "id": 1058209,
                            "productDisplayName": "Phive Rivers Black Leather Printed Sling Bag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/19/1148214913746211456296278568-Flying-Machine-Women-Tops-9021456296278060-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1198157,
                        1058209,
                        1051162,
                        1123787
                    ],
                    "source": "VSBot",
                    "look_id": "5e1981c7-be01-496b-9656-9ebe135e6195",
                    "createdBy": "VSBot",
                    "styleid": 1198157,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Heels": {
                            "brandName": "DressBerry",
                            "discountedPrice": 2899,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1475475/2016/8/30/11472556062921-DressBerry-Women-Heels-9551472556062637-1.jpg",
                            "id": 1475475,
                            "productDisplayName": "Dressberry Women Nude-Coloured Slim Heels"
                        },
                        "Tops": {
                            "brandName": "Trend Arrest",
                            "discountedPrice": 999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1355501/2016/5/19/11463644245064-Trend-Arrest-Blue-Layered-Top-201463644244861-1.jpg",
                            "id": 1355501,
                            "productDisplayName": "Trend Arrest Red Printed Crop Top"
                        },
                        "Handbags": {
                            "brandName": "Butterflies",
                            "discountedPrice": 2399,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/images/style/properties/Butterflies-Light-Brown-Handbag_47f4d3b664c3c6d057ed141a09adf81f_images.jpg",
                            "id": 432012,
                            "productDisplayName": "Butterflies Light Brown Handbag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/19/1148216402305011463644245064-Trend-Arrest-Blue-Layered-Top-201463644244861-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1355501,
                        432012,
                        1051162,
                        1475475
                    ],
                    "source": "VSBot",
                    "look_id": "e0b196eb-938f-461b-8801-51c0bc689792",
                    "createdBy": "VSBot",
                    "styleid": 1355501,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Tshirts": {
                            "brandName": "ONLY",
                            "discountedPrice": 695,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1219840/2016/2/23/11456209850101-ONLY-Women-Tshirts-2071456209849576-1.jpg",
                            "id": 1219840,
                            "productDisplayName": "ONLY Grey Melange T-shirt"
                        },
                        "Flats": {
                            "brandName": "Hush Puppies",
                            "discountedPrice": 1999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1517027/2016/9/29/11475143598031-Hush-Puppies-Women-Pink-Woven-Sandals-7511475143597803-1.jpg",
                            "id": 1517027,
                            "productDisplayName": "Hush Puppies Women Pink Woven Flats"
                        },
                        "Handbags": {
                            "brandName": "Eske",
                            "discountedPrice": 9499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1310090/2016/4/19/11461041848022-Eske-Women-Handbags-2671461041847585-1.jpg",
                            "id": 1310090,
                            "productDisplayName": "Eske Black & White Printed Leather Handbag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/19/1148217136963811456209850101-ONLY-Women-Tshirts-2071456209849576-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1219840,
                        1310090,
                        1051162,
                        1517027
                    ],
                    "source": "VSBot",
                    "look_id": "69c3419c-e5d8-4db6-a537-eaa5bee7f853",
                    "createdBy": "VSBot",
                    "styleid": 1219840,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                },
                {
                    "articles": {
                        "Heels": {
                            "brandName": "Inc 5",
                            "discountedPrice": 1590,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1464737/2016/8/25/11472120695678-Inc-5-Women-Brown-Heels-1951472120695438-1.jpg",
                            "id": 1464737,
                            "productDisplayName": "Inc 5 Women Brown Heels"
                        },
                        "Tops": {
                            "brandName": "DressBerry",
                            "discountedPrice": 1099,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1265633/2016/4/22/11461322917687-DressBerry-Orange-Floral-Print-Top-6471461322917211-1.jpg",
                            "id": 1265633,
                            "productDisplayName": "DressBerry Coral Red Floral Printed Polyester Top"
                        },
                        "Handbags": {
                            "brandName": "GUESS",
                            "discountedPrice": 9999,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1468003/2016/9/7/11473223788964-GUESS-Black-Glossy-Quilted-Shoulder-Bag-3381473223788789-1.jpg",
                            "id": 1468003,
                            "productDisplayName": "GUESS Black Glossy Quilted Shoulder Bag"
                        },
                        "Shorts": {
                            "cropBox": [
                                0.068,
                                0.136,
                                0.932,
                                1
                            ],
                            "brandName": "Blues By the Vanca",
                            "discountedPrice": 1499,
                            "flatshot_url": null,
                            "img_url": "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1051162/2016/8/19/11471595380176-Blues-by-The-Vanca-Blue-Denim-Shorts-1241471595379952-1.jpg",
                            "id": 1051162,
                            "productDisplayName": "Blues by The Vanca Blue Denim Shorts"
                        }
                    },
                    "confidence": 0.5,
                    "collage_url": "http://assets.myntassets.com/w_1080,h_1080/assets/images/2016/12/20/1148219105677511461322917687-DressBerry-Orange-Floral-Print-Top-6471461322917211-1.jpg",
                    "format": "tbaf",
                    "collage_url_flatshot": null,
                    "styleids": [
                        1265633,
                        1468003,
                        1051162,
                        1464737
                    ],
                    "source": "VSBot",
                    "look_id": "42bc5c98-d48b-4102-bb26-8f2bb0b5e9fb",
                    "createdBy": "VSBot",
                    "styleid": 1265633,
                    "hasFlatshots": false,
                    "title": "What you can pair with",
                    "size": 4
                }
            ],
            "format": "tbaf",
            "present_styles": [
                "1051162"
            ],
            "offset": 0,
            "status": {
                "code": 3
            },
            "aregenerated": false
        }
    },
    "encodeImage": ""
}